import { Injectable } from '@nestjs/common';
import { Model, Connection } from 'mongoose';
import { InjectConnection } from '@nestjs/mongoose';
import { KeywordSchema, KeywordDocument } from '../schemas';

@Injectable()
export class KeywordService {
  oauth2Client: any;

  constructor(@InjectConnection() private connection: Connection) {}

  async getTargetKeyword(params): Promise<{ count: number; data: any[] }> {
    const { projectId, offset, query } = params;
    let _filters, _sort;
    if (query) {
      const { sort, filters } = JSON.parse(query);
      _filters = filters;
      _sort = sort;
    }

    const col_name = `keyword_research_${projectId}`;
    await this.connection.model(col_name, KeywordSchema);
    const data = await this.connection
      .model(col_name)
      .find({ targetPage: true }, { _id: false, __v: false })
      .sort({ priority: 1 })
      .collation({ locale: 'en_US', numericOrdering: true })
      .limit(11)
      .skip(Number(offset || 0))
      .exec();
    const count = await this.connection.model(col_name).find(_filters).count();
    return {
      count,
      data,
    };
  }

  async getKeyword(
    params,
  ): Promise<{ count: number; data: any[]; maxPriorityScore: number }> {
    const { projectId, offset, query } = params;
    let _filters, _sort;
    if (query) {
      const { sort, filters } = JSON.parse(query);
      _filters = filters;
      _sort = sort;
    }

    const col_name = `keyword_research_${projectId}`;
    await this.connection.model(col_name, KeywordSchema);
    const data = await this.connection
      .model(col_name)
      .find(_filters, { _id: false, __v: false })
      .sort({ priority: 1 })
      .collation({ locale: 'en_US', numericOrdering: true })
      .limit(11)
      .skip(Number(offset || 0))
      .exec();

    const maxPriorityScoreResult = (
      await this.connection
        .model<KeywordDocument>(col_name)
        .find()
        .sort({ priorityScore: -1 })
        .collation({ locale: 'en_US', numericOrdering: true })
        .limit(1)
        .exec()
    ).pop();
    const maxPriorityScore = maxPriorityScoreResult
      ? Number(maxPriorityScoreResult.priorityScore || 0)
      : 0;
    const count = await this.connection.model(col_name).find(_filters).count();
    return {
      count,
      data,
      maxPriorityScore,
    };
  }

  async updateRow(params, body): Promise<any> {
    const { projectId } = params;
    const _col_name = `keyword_research_${projectId}`;
    await this.connection.model(_col_name, KeywordSchema);
    return await this.connection
      .model(_col_name)
      .updateOne({ url: body.url }, { $set: body }, { upsert: true });
  }
}
