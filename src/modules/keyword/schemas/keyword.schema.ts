import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type KeywordDocument = Keyword & Document;

@Schema({
  strict: false,
})
export class Keyword {
  @Prop(String)
  priority: string;

  @Prop(String)
  contentTitle: string;

  @Prop(String)
  meta: string;

  @Prop(String)
  h1: string;

  @Prop(String)
  urlAction: string;

  @Prop(String)
  url: string;

  @Prop(String)
  category: string;

  @Prop(String)
  sessions: string;

  @Prop(String)
  mainKw: string;

  @Prop(String)
  mainKwPosition: string;

  @Prop(String)
  mainKwVolume: string;

  @Prop(String)
  ctr: string;

  @Prop(String)
  bestKw: string;

  @Prop(String)
  bestKwPosition: string;

  @Prop(String)
  bestKwVolume: string;

  @Prop(String)
  da: string;

  @Prop(String)
  pa: string;

  @Prop(String)
  primaryKeyword: string;

  @Prop(String)
  primaryKeywordVolume: string;

  @Prop(String)
  primaryKeywordRanking: string;

  @Prop(String)
  secondaryKeywords: string;

  @Prop(String)
  journey: string;

  // Backlinks
  @Prop(String)
  firstGoogleResultMainKW: string;

  @Prop(String)
  firstResultContentType: string;

  @Prop(String)
  rdToFirstResult: string;

  @Prop(String)
  rdVelocity: string;

  @Prop(String)
  paOfFirstResult: string;

  @Prop(String)
  daOfFirstResult: string;

  @Prop(String)
  secondGoogleResultMainKW: string;

  @Prop(String)
  secondResultContentType: string;

  @Prop(String)
  rdToSecondResult: string;

  @Prop(String)
  secondRdVelocity: string;

  @Prop(String)
  paOfSecondResult: string;

  @Prop(String)
  daOfSecondResult: string;

  @Prop(String)
  thirdGoogleResultMainKW: string;

  @Prop(String)
  thirdResultContentType: string;

  @Prop(String)
  rdToThirdResult: string;

  @Prop(String)
  thirdRdVelocity: string;

  @Prop(String)
  paOfThirdResult: string;

  @Prop(String)
  daOfThirdResult: string;

  @Prop(String)
  daOpportuniyScore: string;

  @Prop(String)
  paOpportuniyScore: string;

  @Prop(String)
  linksOpportuniyScore: string;

  @Prop(String)
  opportuniyToRank: string;

  @Prop(String)
  priorityScore: string;

  @Prop(String)
  type: string;

  @Prop(String)
  targetPage: boolean;

  @Prop(String)
  recommendedTitle: string;

  @Prop(String)
  recommendedMeta: string;

  @Prop(String)
  recommendedH1: string;

  @Prop(String)
  kwGap: string;

  @Prop(Boolean)
  complete: boolean;
}

export const KEYWORD_COLLECTION_NAME = 'keyword';

export const KeywordSchema = SchemaFactory.createForClass(Keyword);
