import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PerformanceDocument = Performance & Document;

@Schema()
export class Performance {
  @Prop(String)
  url: String;

  @Prop(String)
  fcp: String;

  @Prop(String)
  si: String;

  @Prop(String)
  lcp: String;

  @Prop(String)
  tti: String;

  @Prop(String)
  tbt: String;

  @Prop(String)
  cls: String;

  @Prop(String)
  ps: String;
}

export const PERFORMANCE_COLLECTION_NAME = 'performance';

export const PerformanceSchema = SchemaFactory.createForClass(Performance);
