import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type OrganicDataDocument = OrganicData & Document;

@Schema()
export class OrganicData {
  @Prop(String)
  currentMonth: String;

  @Prop(String)
  lastMonth: String;

  @Prop(String)
  currentMonthSessions: String;

  @Prop(String)
  currentMonthBounceRate: String;

  @Prop(String)
  currentMonthPageviews: String;

  @Prop(String)
  currentMonthPageviewsPerSession: String;

  @Prop(String)
  currentMonthTimeOnPage: String;

  @Prop(String)
  lastMonthSessions: String;

  @Prop(String)
  lastMonthBounceRate: String;

  @Prop(String)
  lastMonthPageviews: String;

  @Prop(String)
  lastMonthPageviewsPerSession: String;

  @Prop(String)
  lastMonthTimeOnPage: String;

  @Prop(String)
  lastYearSessions: String;

  @Prop(String)
  lastYearBoounceRate: String;

  @Prop(String)
  lastYearPageViews: String;

  @Prop(String)
  lastYearPageviewsPerSession: String;

  @Prop(String)
  lastYearTimeOnPage: String;
}

export const ORGANIC_DATA_COLLECTION_NAME = 'OrganicData';

export const OrganicDataSchema = SchemaFactory.createForClass(OrganicData);
