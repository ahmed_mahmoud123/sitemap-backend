const axios = require('axios');
export const getKeywordRanking = async (accuDomain, startDateTS, endDateTS) => {
  let url = 'http://app.accuranker.com/api/v4/domains';
  const date = new Date();
  const endDate = `${date.getFullYear()}-${String(date.getMonth() + 1).padStart(
    2,
    '0',
  )}-${String(date.getDate()).padStart(2, '0')}`; //new Date(+startDateTS).toJSON().split('T')[0];
  date.setDate(date.getDate() - 2);

  const startDate = `${date.getFullYear()}-${String(
    date.getMonth() + 1,
  ).padStart(2, '0')}-${String(date.getDate()).padStart(2, '0')}`; //new Date(+endDateTS).toJSON().split('T')[0];

  let params = {
    fields:
      'id,search_type,keyword,ranks.rank,ranks.landing_page.path,search_locale.conutry_code,search_location,search_locale.region,search_locale.locale,search_locale.locale_short,starred,search_engine.name,search_volume.search_volume,search_volume.avg_cost_per_click,search_volume.competition',
    period_from: startDate,
    period_to: endDate,
  };
  const bodySignParams = Object.entries(params)
    .map(([key, val]) => key + '=' + val)
    .join('&');
  const { data } = await axios.get(
    url + `/${accuDomain}/keywords?` + bodySignParams,
    {
      headers: {
        Authorization: 'Token ' + '0322dbaf8537a8a9855c38cc47756c4be4b533cd',
      },
    },
  );
  return data.map((row) => {
    const firstRank = row.ranks.shift();
    const lastRank = row.ranks.shift();
    let diff = 0;
    if (firstRank && lastRank) {
      diff = Number(lastRank.rank) - Number(firstRank.rank);
    }

    return {
      keyword: row.keyword,
      rank: firstRank.rank,
      diff: diff > 0 ? `${diff}` : `${diff}`,
      url: firstRank.landing_page?.path,
      region: row?.search_locale.region,
      device:
        row.search_type == 2 ? 'Mobile' : row.search_type == 1 ? 'Desktop' : '',
      search_volume: row?.search_volume.search_volume,
    };
  });
};
