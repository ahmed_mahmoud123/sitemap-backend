import { google } from 'googleapis';
import { authGoogle } from '../authGoogle';

export const getPerformance = async (user, viewId) => {
  const oauth2Client = await authGoogle(user);
  const defaults = {
    auth: oauth2Client,
    ids: `ga:${viewId}`,
  };

  const result = await google.analytics('v3').data.ga.get({
    ...defaults,
    'start-date': '2daysAgo',
    'end-date': 'today',
    dimensions: 'ga:hostname , ga:pagePath, ga:medium',
    metrics: 'ga:sessions',
    'max-results': 20,
    filters: 'ga:medium==organic',
    sort: '-ga:sessions',
  });

  const requests = result.data.rows.map((item) => {
    const path = item[1];
    let url = `https://${item[0]}${path}`;
    if (path.indexOf('www') >= 0 || path.indexOf('.') >= 0) {
      url = `https://${path}`;
    }
    return google.pagespeedonline('v5').pagespeedapi.runpagespeed({
      key: 'AIzaSyDXXEBqAJwsaVTZaxJDt0IJYQ5n9kHcISQ',
      url,
    });
  });
  const data = await Promise.all(requests);
  return data.map((item) => {
    return {
      url: item.data.lighthouseResult.requestedUrl,
      fcp:
        item.data.lighthouseResult.audits['first-contentful-paint']
          .displayValue,
      si: item.data.lighthouseResult.audits['speed-index'].displayValue,
      lcp:
        item.data.lighthouseResult.audits['largest-contentful-paint']
          .displayValue,
      tti: item.data.lighthouseResult.audits['interactive'].displayValue,
      tbt:
        item.data.lighthouseResult.audits['total-blocking-time'].displayValue,
      cls:
        item.data.lighthouseResult.audits['cumulative-layout-shift']
          .displayValue,
      ps: item.data.lighthouseResult.categories.performance.score * 100,
    };
  });
};
