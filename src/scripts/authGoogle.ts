import { google } from 'googleapis';
const oauth2Client = new google.auth.OAuth2(
  '1090096002566-i51d9vq4mb5ftsko452nfs84epgifbdr.apps.googleusercontent.com',
  'qOc7aa06ErCtt96mOYT1SXXN',
  'http://localhost:3000/data/token',
);

export const authGoogle = async ({ access_token, refresh_token }) => {
  await oauth2Client.setCredentials({
    access_token,
    refresh_token,
  });
  return oauth2Client;
};
